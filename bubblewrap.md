Since creating a tight sandbox with bubblewrap requires some trial and error, it would probably be most helpful to give a summary of how a bubblewrap script works, and some tips on how to write them (the [Arch Wiki](https://wiki.archlinux.org) is a goood resource).

- check to see if a source is a symlink (/bin is usually a symlink to /usr/bin for example)
- run your sandbox on a shell, such as `bash`, for easy testing of what's available in the sandbox
- files and directories can be effectively blacklisted (eg. `bwrap --bind / / --tmpfs /proc bash` creates a sandbox with a blank /proc)
- binding a file or directory when the parent is not already bound will effectively whitelists, allowing it to be accessed in the sandbox (eg. `bwrap --ro-bind /usr /usr --ro-bind /lib /lib --ro-bind /lib64 /lib64  --ro-bind $HOME/.config $HOME/.config bash` creates a sandbox where $HOME/.config is readable, even though the rest of $HOME is not)
